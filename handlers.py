import json
from tornado import gen
from tornado.web import RequestHandler
from tornado.websocket import WebSocketHandler
from passlib.hash import bcrypt
from uuid import uuid4

class BaseHandler(RequestHandler):
    def get_db(self):
        return self.settings['db']

    def json(self, data):
        self.set_header('Content-Type', 'application/json')
        self.write(json.dumps(data))


class MainHandler(BaseHandler):
    def get(self):
        self.render('index.html')


class GameHandler(WebSocketHandler):
    pass


class LoginHandler(BaseHandler):
    @gen.coroutine
    def post(self):
        data = json.loads(self.request.body.decode())
        login = data['login'].lower()
        password = data['password']
        if not login or not password:
            self.json({
                'error': 'Invalid input'
            })
            self.finish()
            return
        db = self.get_db()
        user = yield db.users.find_one({
            'login': login
        })
        if not user or not bcrypt.verify(password, user['password']):
            self.json({
                'error': 'No such user'
            })
            self.finish()
            return
        access_token = str(uuid4())
        db.users.update({
            'login': login
            },{
            '$set': {
                'access_token': access_token
            }
        })
        self.json({
            'response': {
                'access_token': access_token
            }
        })
        self.finish()

class RegisterHandler(BaseHandler):
    @gen.coroutine
    def post(self):
        data = json.loads(self.request.body.decode())
        login = data['login'].lower()
        password = data['password']
        db = self.settings['db']
        user = yield db.users.find_one({
            'login': login
        })
        if user:
            self.json({
                'error': {
                    'message': 'User with same login already exist'
                    }
            })
            self.finish()
            return
        res = yield db.users.insert({
            'login': login,
            'password': bcrypt.encrypt(password)
        })
        if res:
            self.json({
                'response': {
                    'message': 'Register success'
                }
            })
        else:
            self.json({
                'error':  {
                    'message': 'Failed to register user'
                }
            })
        self.finish()
