var App = angular.module('App', ['ngRoute'])
    .config(function($routeProvider) {
       $routeProvider
        .when('/', {
                controller: 'MainController',
                templateUrl: 'static/partials/main.html'
        })
        .when('/login', {
            controller: 'LoginController',
            templateUrl: 'static/partials/login.html'
        })
        .when('/register', {
            controller: 'RegisterController',
            templateUrl: 'static/partials/register.html'
        })
        .when('/gameScreen', {
            controller: 'gameScreenController',
            templateUrl: 'static/partials/gameScreen.html'
        })
        .when('/gameRate', {
            controller: 'gameRateController',
            templateUrl: 'static/partials/gameRate.html'
        })
        .otherwise({
            redirectTo: '/'
        });
    })
    .factory('AuthService', function($q, $timeout, $http) {
        var token = null;
        var isLoggedIn = function() {
            return !! token;
        };
        var login = function(login, password) {
            var deffered = $q.defer();
            $http.post('/login', {
                login: login,
                password: password
            }).success(function(data, status) {
                if (status == 200 && data.response) {
                    token = data.access_token;
                    deffered.resolve();
                }
                token = null;
                deffered.reject();
            }).error(function(data) {
                token = null;
                deffered.reject();
            });
            return deffered.promise;
        };
        var logout = function() {
            token = null;
        };

        var register = function(login, password) {
            var deffered = $q.defer();
            $http.post('/register', {
                login: login,
                password: password
            }).success(function(data, status) {
                if (status == 200) {
                    deffered.resolve();
                }
                token = null;
                deffered.reject();
            }).error(function(data) {
                deffered.reject();
            });
            return deffered.promise;
        };
        return {
            isLoggedIn: isLoggedIn,
            login: login,
            logout: logout,
            register: register
        };
    })
    .controller('MainController', function($scope) {

    })
    .controller('LoginController', function($scope, $location, AuthService) {
        $scope.doLogin = function($event) {
            if ($event) {
                $event.preventDefault();
            }
            AuthService.login($scope.login, $scope.password)
                .then(function() {
                    $location.path('/');
                })
                .catch(function(message) {
                    $scope.message = message;
                    console.log("Auth failed");
                });

        };
    })
    .controller('RegisterController', function($scope, $location, AuthService) {
        $scope.doRegister = function($event) {
            if ($event) {
                $event.preventDefault();
            }
            AuthService.register($scope.login, $scope.password)
                .then(function() {
                    $location.path('/');
                })
                .catch(function(message) {
                    $scope.message = message;
                });


        };
    })
    .controller('gameScreenController',function ($scope) {
        // body...
    })
    .controller('gameRateController',function ($scope) {
        // body...
    })
