#!/usr/bin/env python3

import motor
from tornado.web import Application
from tornado.ioloop import IOLoop
from handlers import *


config = {
    'autoreload': True,
    'debug': True,
    'cookie_secret': 'ad0f3ef6-4b77-11e5-a479-03e492e6e10a',
    'template_path': 'templates',
    'static_path': 'static'
}

db = motor.MotorClient().linguabattle

app = Application([
    (r'/', MainHandler),
    (r'/login', LoginHandler),
    (r'/register', RegisterHandler),
    (r'/ws', GameHandler)
    ],
    db=db,
    **config
)
app.listen(1337)

print("Server started...")
IOLoop.current().start()
